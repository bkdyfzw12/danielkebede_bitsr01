document.onscroll = () => {
  let nav = document.getElementById("n");

  if (window.scrollY >= 20) {
    nav.style.backgroundColor = "grey";
    nav.style.opacity = 1;
  } else {
    nav.style.backgroundColor = "rgb(26, 23, 61)";
  }
};
const navSlide = () => {
  const toggle = document.querySelector(".toggle-btn");
  const nav = document.querySelector(".nav-link");
  toggle.addEventListener("click", () => {
    nav.classList.toggle("nav-active");
  });
};
navSlide();
